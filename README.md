# `nixos-installer`

## Usage

``` shell
nix run --extra-experimental-features 'nix-command flakes' gitlab:niklashh/nixos-installer -- --help
```

### Example

``` shell
nix run --extra-experimental-features 'nix-command flakes' gitlab:niklashh/nixos-installer -- /dev/sda -vi
```
