#!/usr/bin/env sh

# * nixos-installer helper script
#
# ** Goals
# - installs minimal nixos
# - interactive and non-interactive modes
# - customizable with:
#   - supports BIOS boot loader for Hetzner
#   - supports EFI boot loader
#   - generates SSH host key and converts to age
#   - TODO install existing flake
#   - TODO ssh keys
#   - TODO custom swap size
# - TODO warn if disk is mounted after failure

# Start helpers

BOLD=$(tput bold)
CYAN=$(tput setaf 6)
GRAY=$(tput setaf 8)
PINK=$(tput setaf 9)
ORANGE=$(tput setaf 11)
NC=$(tput sgr0)

function debug() {
    if [[ "${VERBOSE}" == 1 ]]; then
        echo -e "${BOLD}${GRAY}[*]${NC}${GRAY} $@${NC}"
    fi
}

function info() {
    echo -e "${BOLD}${CYAN}[*]${NC} $@"
}

function error() {
    echo -e "${BOLD}${PINK}[-]${NC} $@"
}

function prompt() {
    echo -ne "${BOLD}${ORANGE}[?]${NC} $@"
}

set -o errexit -o pipefail -o noclobber -o nounset -o functrace -o errtrace

function failure() {
  local lineno=$1
  local msg=$2
  error "[$0:$lineno]: $msg"
}
trap 'failure ${LINENO} "$BASH_COMMAND"' ERR

# End of helpers

# FIXME should be configurable
AUTHORIZED_KEYS='["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK5eM8J73yRbWvoieUufejFpZq2OE7/93zc51G44r7qy niklash"]'

function usage() {
    echo "Usage:"
    echo "  nixos-installer [options] <disk> [stages]"
    echo
    echo "Install NixOS from a Live NixOS environment."
    echo
    echo "Options:"
    echo " -l, --legacy         Use a legacy BIOS partition for the bootloader."
    echo " -v, --verbose        Enable verbose logging."
    echo " -i, --interactive    Wait for confirmation between steps."
    echo "                      Note: answering 'no' will not stop the script."
    echo " -C, --no-cleanup     Don't unmount after finishing."
    echo " -h, --help           Display this help message."
    echo
    echo "Stages:               Stages are run in the order they are provided. By default"
    echo "                      all of the following stages are run in the order below."
    echo "  configure_nix       Appends experimental-features to installer's /etc/nix/nix.conf"
    echo "  partition_disks     Partitions the disk. Creates one boot and one root partition."
    echo "  create_swap         Creates and enables a swap file at /mnt/.swapfile."
    echo "  configure_nixos     Generates a minimal NixOS configuration at /mnt/etc/nixos/."
    echo "  generate_keys       Generate SSH host key and convert to age."
    echo "  install_nixos       Installs the generated NixOS configuration."
    echo "  cleanup             Disables swap and unmounts partitions."
}

# Reads variables such as VERSION_ID
source /etc/os-release

function append() {
    sed -i "/^$(echo "$1" | sed -e 's/[]\/()$*.^|[]/\\&/g')$/d" "$2"
    echo "$1" >>"$2"
}

function interact() {
    prompt "$1 [Y/n] "
    read -rsn 1 input
    echo $input
    if [[ "$input" == "n" ]]; then
        info "Skipping..."
        return 1
    fi
}

function configure_nix() {
    if [[ "${INTERACTIVE}" == 1 ]]; then
        interact "Enable experimental commands in this Nix system?" || return 0
    fi

    info "Adding experimental features to /etc/nix/nix.conf"
    append "experimental-features = nix-command flakes" /etc/nix/nix.conf

    debug "Configure Nix done"
}

function wait_for_file() {
    count=0
    while [ ! -L "$1" ]
    do
        if ((count > 10))
        then
            error "unable to find '$1'"
            exit 1
        fi
        sleep 1
        count=$((count++))
    done
}

function partition_disks() {
    PARTITION_LAYOUT_LEGACY=$(cat <<-EOF
label: gpt
${DISK}1 : start=2048, size=2048, type=21686148-6449-6E6F-744E-656564454649, name="boot"
${DISK}2 : start=,                type=0FC63DAF-8483-4772-8E79-3D69D8477DE4, name="root"
EOF
                           )

    PARTITION_LAYOUT_UEFI=$(cat <<-EOF
label: gpt
${DISK}1 : size=512MiB, type=C12A7328-F81F-11D2-BA4B-00A0C93EC93B, name="boot"
${DISK}2 : start=,      type=0FC63DAF-8483-4772-8E79-3D69D8477DE4, name="root"
EOF
                         )

    case "$1" in
        legacy) PARTITION_LAYOUT="${PARTITION_LAYOUT_LEGACY}";;
        uefi) PARTITION_LAYOUT="${PARTITION_LAYOUT_UEFI}";;
        *) error "partition_disks called with unexpected partition layout: '$1'"
           exit 1
           ;;
    esac
    debug "Planned partition layout is:\n${PARTITION_LAYOUT}"
    if [[ "${INTERACTIVE}" == 1 ]]; then
        interact "Partition and mount the disk?" || return 0
    fi

    info "Partitioning '$DISK'"
    echo "$PARTITION_LAYOUT" |sfdisk --wipe always --wipe-partitions always "$DISK"

    debug "Creating an ext4 filesystem on ${DISK}2"
    mkfs.ext4 -L root "${DISK}2"

    debug "Probing root partition"
    partprobe "${DISK}2"
    wait_for_file /dev/disk/by-label/root

    debug "Mounting /dev/disk/by-label/root"
    mount /dev/disk/by-label/root /mnt

    if [[ "$1" == "uefi" ]]; then
        mkfs.fat -F32 -n boot "${DISK}1"
        debug "Probing boot partition"
        partprobe "${DISK}1"
        wait_for_file /dev/disk/by-label/boot

        mkdir /mnt/boot
        mount /dev/disk/by-label/boot /mnt/boot
    fi

    debug "Partition disks done"
}

function create_swap() {
    if [[ "${INTERACTIVE}" == 1 ]]; then
        interact "Create a swap file in /mnt/.swapfile?" || return 0
    fi

    info "Creating a swap file in /mnt/.swapfile"
    dd if=/dev/zero of=/mnt/.swapfile bs=1024 count=2097152

    debug "Setting /mnt/.swapfile permissions"
    chmod 600 /mnt/.swapfile

    debug "Initializing /mnt/.swapfile as swap"
    mkswap /mnt/.swapfile

    debug "Enabling /mnt/.swapfile for swapping"
    swapon /mnt/.swapfile

    debug "Create swap done"
}

function generate_keys() {
    debug "Generating SSH host key /mnt/etc/ssh/ssh_host_ed25519_key"
    ssh-keygen -f /mnt/etc/ssh/ssh_host_ed25519_key -q -N ""

    debug "Converting SSH host key to age"
    AGE=$(nix run nixpkgs#ssh-to-age < /mnt/etc/ssh/ssh_host_ed25519_key.pub)

    info "Age encoding of /mnt/etc/ssh/ssh_host_ed25519_key.pub: ${BOLD}${AGE}${NC}"

    debug "Generate keys done"
}

function configure_nixos() {
    BOOT_LOADER_LEGACY=$(cat <<-EOF
  # Use the GRUB 2 boot loader.
  boot.loader.grub = {
    enable = true;
    device = "${DISK}"; # or "nodev" for efi only
  };
EOF
                      )

    BOOT_LOADER_UEFI=$(cat <<-EOF
  # Use the systemd-boot boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
EOF
                    )

    case "$1" in
        legacy) BOOT_LOADER="${BOOT_LOADER_LEGACY}";;
        uefi) BOOT_LOADER="${BOOT_LOADER_UEFI}";;
        *) error "configure_nixos called with unexpected partition layout: '$1'"
           exit 1
           ;;
    esac

    NIXOS_CONFIGURATION=$(cat <<-EOF
{ config, lib, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

${BOOT_LOADER}

  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  services.openssh.enable = true;
  users.users.root.openssh.authorizedKeys.keys = ${AUTHORI};
  system.stateVersion = "${VERSION_ID}"; # Did you read the comment?
}
EOF
                       )

    if [[ "${INTERACTIVE}" == 0 ]] || interact "Run nixos-generate-config?"; then
        info "Generating initial configuration"
        nixos-generate-config --root /mnt
    fi

    if [[ "${INTERACTIVE}" == 0 ]]; then
        debug "New configuration is:\n${NIXOS_CONFIGURATION}"
    else
        info "New configuration is:\n${NIXOS_CONFIGURATION}"
    fi
    if [[ "${INTERACTIVE}" == 0 ]] || interact "Use the new configuration?"; then
        echo "${NIXOS_CONFIGURATION}" >| /mnt/etc/nixos/configuration.nix
    fi

    if [[ "${INTERACTIVE}" == 1 ]]; then
        function onresume() {
            info "Continue by pressing [Enter]."
            read
        }
        trap onresume CONT
        info "Please check /mnt/etc/nixos/configuration.nix etc. by pressing [CTRL-Z]. To return, run 'fg'. To continue right away, press [Enter]."
        ! read # read seems to fail after resuming because of the trap
        trap - CONT
    fi

    debug "Configure NixOS done"
}

function install_nixos() {
    if [[ "${INTERACTIVE}" == 1 ]]; then
        interact "Run nixos-install?" || return 0
    fi

    info "Installing NixOS"
    nixos-install --no-root-passwd --no-channel-copy

    debug "Install NixOS done"
}

function cleanup() {
    if [[ "${INTERACTIVE}" == 1 ]]; then
        interact "Disable swap?" || return 0
    fi

    info "Disabling swap and unmounting"
    swapoff /mnt/.swapfile

    if [[ "${INTERACTIVE}" == 1 ]]; then
        interact "Unmount /mnt?" || return 0
    fi
    debug "Unmounting /mnt"
    umount -R /mnt
}

function main() {
    # Command line argument parsing source <https://gist.github.com/bobpaul/ecd74cdf7681516703f20726431eaceb>
    ! getopt --test > /dev/null
    if [[ "${PIPESTATUS[0]}" -ne 4 ]]; then
        error "GNU's enhanced getopt is required to run this script\n    You can usually find this in the util-linux package"
        exit 1
    fi

    SHORT=-hviC
    LONG=help,verbose,interactive,no-cleanup

    # - Temporarily store output to be able to check for errors.
    # - Activate advanced mode getopt quoting e.g. via "--options".
    # - Pass arguments only via   -- "$@"   to separate them correctly.
    # - getopt auto-adds "--" at the end of ${PARSED}, which is then later set to
    #   "$@" using the set command.
    ! PARSED=$(getopt --options="${SHORT}" \
                      --longoptions="${LONG}" \
                      --name "$0" \
                      -- "$@")
    if [[ "${PIPESTATUS[0]}" -ne 0 ]]; then
        # e.g. $? == 1
        #  then getopt has complained about wrong arguments to stdout
        exit 2
    fi
    # Use eval with "$PARSED" to properly handle the quoting
    # The set command sets the list of arguments equal to ${PARSED}.
    eval set -- "${PARSED}"

    # TODO ssh keys
    HELP=0
    VERBOSE=0
    INTERACTIVE=0
    CLEANUP=1
    LEGACY=0
    declare -a STAGES
    STAGES=("configure_nix" "partition_disks" "create_swap" "generate_keys" "configure_nixos" "install_nixos")
    declare -a POSITIONAL
    DISCARD_OPTS_AFTER_DOUBLEDASH=0 # 1=Discard, 0=Save opts after -- to ${EXTRA_ARGS}
    dashes=0 # flag to track if we've parsed '--'
    while [[ $# -gt 0 ]]; do
        case "$1" in
            -l|--legacy) LEGACY=1;;
            -h|--help) HELP=1;;
            -v|--verbose) VERBOSE=1;;
            -i|--interactive) INTERACTIVE=1;;
            -C|--no-cleanup) CLEANUP=0;;
            --) dashes=1
                if [[ ${DISCARD_OPTS_AFTER_DOUBLEDASH} -eq 1 ]]; then break; fi
                ;;
            *)  #store positional arguments until we reach the dashes, then store as extra
                if [[ $dashes -eq 0 ]]; then POSITIONAL+=("$1");
                else EXTRA_ARGS+=("$1"); fi
                ;;
        esac
        shift # Expose the next argument
    done
    set -- "${POSITIONAL[@]}"

    if [[ "${HELP}" == 1 ]]; then
        usage
        exit 0
    fi
    if [[ "${CLEANUP}" == 1 ]]; then
        STAGES+=("cleanup")
    fi
    if [[ "${LEGACY}" == 1 ]]; then
        INSTALLATION_TYPE="legacy"
    else
        INSTALLATION_TYPE="uefi"
    fi

    if [[ $# == 0 ]]; then
        error "Missing required positional argument DISK."
        echo
        usage
        exit 1
    fi
    DISK="$1"
    shift
    if [[ $# > 0 ]]; then
        # Process stages
        STAGES=()
        while [[ $# > 0 ]]; do
            STAGES+=("$1")
            shift
        done
    fi

    # Log all output to a file
    LOGFILE=nixos-installer-$(date -Is).log
    exec > >(tee -i "${LOGFILE}")
    exec 2>&1

    info "Running the following stages: ${STAGES[@]}"
    for stage in ${STAGES[@]}; do
        case "$stage" in
            "configure_nix") configure_nix;;
            "partition_disks") partition_disks ${INSTALLATION_TYPE};;
            "create_swap") create_swap;;
            "generate_keys") generate_keys;;
            "configure_nixos") configure_nixos ${INSTALLATION_TYPE};;
            "install_nixos") install_nixos;;
            "cleanup") cleanup;;
            *) error "Invalid stage '$stage'"
               exit 1
               ;;
        esac
        echo
    done

    if [[ ! -z "${AGE:-}" ]]; then
        info "Age encoding of /mnt/etc/ssh/ssh_host_ed25519_key.pub: ${BOLD}${AGE}${NC}"
    fi
    info "Installer logs saved to '${LOGFILE}'"
}

main "$@"
